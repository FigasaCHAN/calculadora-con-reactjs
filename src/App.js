import "./App.css";
import GridButton from "./GridButton";
import LabelCalculator from "./LabelCalculator";

function App() {
    return (
        <div className="App">
            <div id="header">
                <h1 id="title"> Calculadora con ReactJS</h1>
                <LabelCalculator/>
            </div>
            <GridButton/>
        </div>
    );
}

export default App;
