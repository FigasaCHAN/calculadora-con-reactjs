import "./LabelCalculator.css";
import React from "react";
import { store } from "./state/store";
import { ACTION_INPUT_VALUE_CHANGE, ACTION_CLICK_INPUT_NUM } from "./state/actions";

export default class LabelCalculator extends React.Component{
    
    state= {
        num1: 0,
        num2: 0,
        operation: '+',
        result: 0,
        lastInputSelected: null,
    }

    componentDidMount(){
        store.subscribe( () => {
        let {num1,num2,operation,result,lastInputSelected}= store.getState();
        this.setState({num1,num2,operation,result,lastInputSelected})        
        })
    }
    render(){
        let {operation,result}= this.state;
        console.log("Render");
        console.log(this.state);
        let refInput1,refInput2;
        return(
            <div className="LabelCalculator">
                {/* <label>{num1}</label>
                <label>{operation}</label>
                <label>{num2}</label> */}
                <input id={"num1"} ref={input => refInput1=input} type="number" defaultValue={0} onClick={()=> store.dispatch(ACTION_CLICK_INPUT_NUM(refInput1) ) } onChange={(obj) => store.dispatch(ACTION_INPUT_VALUE_CHANGE(obj['target']['value']))}></input>
                <label>{operation}</label>
                <input id= {"num2"} ref={input => refInput2=input} type="number" defaultValue={0} onClick={()=> store.dispatch(ACTION_CLICK_INPUT_NUM(refInput2) ) } onChange={(obj) => store.dispatch(ACTION_INPUT_VALUE_CHANGE(obj['target']['value']))}></input>
                <label>=</label>
                <label>{result}</label>
                <hr/>
            </div>
        )
    }

}
