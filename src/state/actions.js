import {
    SUM_NUMBER,
    MUL_NUMBER,
    SUBS_NUMBER,
    DIV_NUMBER,
    INPUT_VALUE_CHANGE,
    CLICK_INPUT_NUM,
    PRESS_BTN_NUM,
} from "./types";

export const ACTION_SUM = () => {
    console.log("Action SUM");
    return {
        type: SUM_NUMBER,
    };
};
export const ACTION_MUL = () => {
    console.log("Action MUL");
    return {
        type: MUL_NUMBER,
    };
};
export const ACTION_SUBS = () => {
    console.log("Action SUBS");
    return {
        type: SUBS_NUMBER,
    };
};
export const ACTION_DIV = () => {
    console.log("Action div");
    return {
        type: DIV_NUMBER,
    };
};
export const ACTION_INPUT_VALUE_CHANGE = (e) => {
    console.log("Action INPUT_VALUE_CHANGE");
    return {
        type: INPUT_VALUE_CHANGE,
        number: Number(e),
    };
};
export const ACTION_CLICK_INPUT_NUM = (e)=>{
    console.log("Action CLICK_INPUT_NUM");
    return {
        type: CLICK_INPUT_NUM,
        lastInputSelected: e
    };
}
export const ACTION_PRESS_BTN_NUM= (num)=>{
    console.log("Action PRESS_BTN_NUM");
    return{
        type: PRESS_BTN_NUM,
        numberPress: num
    };
}