import { SUM_NUMBER,MUL_NUMBER,SUBS_NUMBER,DIV_NUMBER, INPUT_VALUE_CHANGE, CLICK_INPUT_NUM, PRESS_BTN_NUM } from "./types";

export const LABEL_CALCULATOR_REDUCER= (state,action)=>{
    console.log("Reducer");
    console.log(state);
    let{num1,num2,operation,result,lastInputSelected}= state;
    let newState= {num1: num1,
    num2: num2,
    operation: operation,
    result: result,
    lastInputSelected: lastInputSelected
    };
    switch (action['type']) {
        case SUM_NUMBER:
            newState["operation"]= "+"
            newState["result"]= solver(num1,num2,"+");
            return newState;
        case MUL_NUMBER:
            newState["operation"]= "x"
            newState["result"]= solver(num1,num2,"x");
            return newState;
        case SUBS_NUMBER:
            newState["operation"]= "-"
            newState["result"]= solver(num1,num2,"-");
            return newState;
        case DIV_NUMBER:
            newState["operation"]= "/"
            newState["result"]= solver(num1,num2,"/");
            return newState;
        case INPUT_VALUE_CHANGE:
            let inputChanged= newState['lastInputSelected']['id'];
            newState[inputChanged]= Number(action['number']);
            return newState;
        case CLICK_INPUT_NUM:
            newState["lastInputSelected"]= action['lastInputSelected'];
            return newState;
        case PRESS_BTN_NUM:
            if(lastInputSelected){
                let inputChanged= newState['lastInputSelected']['id'];
                let inputValue= newState["lastInputSelected"]['value'];
                let inputValueFinal= Number (inputValue.toString() + action['numberPress'].toString() );
                newState["lastInputSelected"]['value']= inputValueFinal; /* text (or value) of the input */
                newState[inputChanged]= inputValueFinal; /* state update */
                newState["lastInputSelected"].focus();
                return newState;
            }
            return state;
        default:
            return state;
    }
}
function solver(num1,num2,operation){
    switch (operation) {
        case "+":
            return Number(num1 + num2);
        case "-":
            return Number(num1-num2);
        case "x":
            return Number(num1*num2);
        case "/":
            let number= num2===0? 0 : Number(num1/num2); /*Cannot divide by zero*/
            return number;
        default:
            break;
    }
}
