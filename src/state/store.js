import { createStore } from "redux";
import { LABEL_CALCULATOR_REDUCER } from "./reducers";

export const store = createStore(
    LABEL_CALCULATOR_REDUCER,{
        num1: 0,
        num2: 0,
        operation: '+',
        result: 0
    }
)