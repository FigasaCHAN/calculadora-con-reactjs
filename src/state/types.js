export const SUM_NUMBER = "SUM_NUMBER";
export const MUL_NUMBER = "MUL_NUMBER";
export const SUBS_NUMBER = "SUBS_NUMBER";
export const DIV_NUMBER = "DIV_NUMBER";
export const INPUT_VALUE_CHANGE = "INPUT_VALUE_CHANGE";
export const CLICK_INPUT_NUM = "CLICK_INPUT_NUM";
export const PRESS_BTN_NUM = "PRESS_BTN_NUM";
