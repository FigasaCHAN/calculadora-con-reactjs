import {
    ACTION_SUM,
    ACTION_MUL,
    ACTION_SUBS,
    ACTION_DIV,
    ACTION_PRESS_BTN_NUM,
} from "./state/actions";
import { store } from "./state/store";

export default function ButtonC(props) {
    const strText = props["text"];
    const strType = props["type"];
    let strStyle = selectStyle(strType);
    let strCol = selectCol(strType);
    let funcEvent = selectEvent(strText);
    return (
        <div className={strCol} style={{}}>
            <button
                type="button"
                className={strStyle}
                style={{ width:"100%"  ,height: "100%", padding:"5% 0"}}
                onClick={() => {
                    store.dispatch(funcEvent());
                }}
            >
                {strText}
            </button>
        </div>
    );
}
function selectStyle(strType) {
    switch (strType) {
        case "operation":
            return "btn btn-primary rounded-pill";
        case "number":
            return "btn btn-secondary rounded-pill";
        default:
            console.log("Entry to default");
            break;
    }
}
function selectCol(strType) {
    if (strType === "number") {
        return "col-4";
    } else {
        return "col-2";
    }
}
function selectEvent(strText) {
    switch (strText) {
        case "+":
            return () => ACTION_SUM();
        case "x":
            return () => ACTION_MUL();
        case "-":
            return () => ACTION_SUBS();
        case "/":
            return () => ACTION_DIV();

        default:
            return selectEventNumber(strText);
    }
}
function selectEventNumber(strText){
    let numberParse= parseInt(strText);
    if(!Number.isNaN(numberParse)){
        return ()=> ACTION_PRESS_BTN_NUM(numberParse);
    }
    else{
        return null;
    }
}
