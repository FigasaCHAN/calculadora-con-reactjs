import "./GridButton.css";
import ButtonC from "./ButtonC";

export default function GridButton() {
    return createButtons();
}

function createButtons() {
    let arrayObj = [];
    for (let i = 1; i < 10; i++) {
        let obj = { text: i, type: "number" };
        arrayObj.push(obj);
    }
    console.log(arrayObj);
    let arraySeparate = [];
    for (let i = 1; i < 4; i++) {
        arraySeparate.push(arrayObj.splice(0, 3));
    }
    console.log(arraySeparate);
    arraySeparate.reverse();
    return (
        <div className="GridButton">
            <div className="container">
                {createRow(arraySeparate[0])}
                {createRow(arraySeparate[1])}
                {createRow(arraySeparate[2])}
                {createElemZero()}
            </div>
        </div>
    );
}

function createRow(arrayElem) {
    /* return elem react  */
    let arrayElemsReact = [];
    arrayElem.forEach((e) => {
        arrayElemsReact.push(
            <ButtonC key={e["text"]} text={e["text"]} type={e["type"]} />
        );
    });
    return <div className="row">{arrayElemsReact}</div>;

    // return <div className="row">{arrayElemsReact}</div>;
}

function createElemZero() {
    let sum = { text: "+", type: "operation" };
    let mul = { text: "x", type: "operation" };
    let zero = { text: "0", type: "number" };
    let subs = { text: "-", type: "operation" };
    let div = { text: "/", type: "operation" };
    return createRow([sum, mul, zero, subs, div]);
}
